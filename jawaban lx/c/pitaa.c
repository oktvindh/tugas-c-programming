#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(){
    int x, y, z, i, j, N;
    scanf("%d", &x);
    for(i=1; i<=x; i++){
        scanf("%d", &y);
        z=y;
        z*=2; z--; 
        for(j=1; j<=z; j++){
            for(N=1; N<=z; N++){
                if((j==y)&&(N==y)) printf("*");
                else if(j<=y){
                    if ((N>=j)&&(N<=y)) printf("*");
                    else if((N<j)&&(N<y)) printf(" ");
                    }
                else if(j>=y){
                    if ((N<=j)&&(N>=y)) printf("*");
                    else if((N<j)&&(N<y)) printf(" ");
                }
                else printf(" ");
                
            }
            printf("\n");
        }
        if(i<x) printf("\n");
    }
    return 0;
}