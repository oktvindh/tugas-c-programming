#include <stdio.h>
#include <math.h>
#include <stdlib.h>

int main(){
    int n, a, i, j;
    scanf("%d", &a);
    for(j=0; j<a; j++){
        int hasil=0;
        scanf("%d", &n);
        for(i=1; i<=sqrt(n); i++){
            if(n%i==0){
                if(n/i==i) hasil+=1;
                else hasil+=2;
            }
        }
        printf("%d\n", hasil);
    }
    return 0;
}