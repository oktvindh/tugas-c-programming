#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main() 
{
    long N;
    scanf("%d", &N);
    int terbesar = N % 1, hasil, catat, i = 2;
    
    while (i < N)
    {
        hasil = N % i;
        if (hasil > terbesar){
            catat = i;
            terbesar = hasil;
        }
        i++;
    }
    
    printf("%d\n", catat);
    return 0;
}