#include <stdio.h>
int main() 
{
    int a, b, c = 0;
    scanf("%d %d", &a, &b);
    while (a <= b)
    {
        c += a*a;
        a++;
    }
    printf("%d\n", c);
    return 0;
}