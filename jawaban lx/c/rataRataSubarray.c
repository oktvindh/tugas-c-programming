#include <stdio.h>
#include <math.h>

int main(){
    int N, K;
    scanf("%d", &N);
    scanf("%d", &K);

    // baca array
    int arr[N];
    for (int i = 0; i < N; i++) {
        scanf("%d", &arr[i]);
    }

    // total 
    int total = 0;
    for (int i = 0; i < N; i++) {
        total += arr[i];
    }

    // rerata

    int rerata = 0;
    for (int i = 0; i < N; i++)
    {
        if(arr[i]*N >= K) {
            rerata++;
        }
    }
    printf("%d\n", rerata);
    return 0;
}



