#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(){
    int a, baris, kolom;
    scanf("%d", &a);
    for(int i=1; i<=a; i++){
        scanf("%d %d", &baris, &kolom);
        baris = baris*3+1;
        kolom = kolom*3+1;
        for(int j=1; j<=baris; j++){
            for(int k=1; k<=kolom; k++){
                if((j-1)%3==0 || (k-1)%3==0) printf("*");
                else printf(".");
            }
            printf("\n");
        }
        if(i<a) printf("\n");
    }
}