#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(){
    int a, b, i, j, tes;
    scanf("%d %d", &a, &b);
    for(i=1; i<=a; i++){
        for(j=1; j<=b; j++){
            tes=(i%2)+(j%2);
            switch (tes){        
                case 0: printf("#"); break;
                case 1: printf("$"); break;
                case 2: printf("*"); break;
            }
        }
        printf("\n");
    }
    return 0;
}