#include<stdio.h>

int main()
{
    int n,kiri,kanan,tengah;
    int cek = 1;
    scanf("%d",&n);
    scanf("%d %d",&kiri,&tengah);
    for (int i = 2;i < n;i ++)
    {
        scanf("%d",&kanan);
        if (tengah < kiri && tengah < kanan)
        {
            cek = 0;
            break; // break dipakai saat kita tidak butuh input sisa disaat
                   // input ke-k memenuhi kondisi ini (k < n)
        }
        kiri = tengah;
        tengah = kanan;
    }
    if (!cek)
    {
        printf("%d: tidak",n);
    }
    else printf("%d: cantik",n);
    puts("");
    return 0;
}