#include <stdio.h>
int main() 
{
    int n, nilai, c = 0, sum = 0, ganjil = 0;
    double rataRata;
    scanf("%d", &n);
    while(c++ < n)
    {
        scanf("%d", &nilai);
        if (c % 2 == 1) 
        {
            sum += nilai;
            ganjil++;
        }
        
    }
    rataRata = (double)sum/ganjil;
    printf("%.2lf\n", rataRata);
    return 0;
}