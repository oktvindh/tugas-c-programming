#include <stdio.h>
#include <math.h>
#include <stdlib.h>

int main(){
    int a, b, i, c = 0;

    scanf("%d %d", &a, &b);
    printf("%d", b);
    c = b;

    for (i = 2; i <= a; i++){
        scanf("%d", &b);
        if (b > c){
            c = b;
            printf(" %d", c);
        }
    }
    printf("\n");
    return 0;
}