#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(){
    int a, b, cek, tes;
    scanf("%d", &a);
    for(int i=0; i<a; i++){
        scanf("%d", &cek);
        tes=1;
        while(cek != -9){
            scanf("%d", &b);
            if(b==cek) tes++;
            else{
                printf("%d(%d)", cek, tes);
                tes=1;
                cek=b;
            }
        }
        printf("\n");
    }
}